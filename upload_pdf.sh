#this script uploads pdf uploads them to eos
echo "Uploading test pdf of current commit to eos"
#make a copy to upload to eos, labelled by git commit hash
last_commit=$(git rev-parse --verify --short HEAD)
UPLOAD_DIR="pdf_$last_commit"
mkdir $UPLOAD_DIR
cp $PDF_FILE $UPLOAD_DIR
#upload to eos
xrdcp -r $UPLOAD_DIR $CERN_BOX_URL


