#preamble
cat > $TEX_FILE << EOF
\documentclass{article}
\title{$TITLE}
\author{$AUTHOR}
\begin{document}
\maketitle
EOF

#content
while IFS= read -r line; do
    printf "%-3s %-10s %-20s\n\n" "$line" '\newline' >> $TEX_FILE
done < "$MOVIE_SCRIPT_TXT"

#end
echo "\end{document}" >> $TEX_FILE
